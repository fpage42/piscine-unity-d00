﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour {

	float size = 50;
	int soufle = 70;
	int wait = 0;
	bool run = true;

	void Update () {
		if (run) {
			if (size <= 0) {
				Debug.Log ("Balloon life time: " + (int)Time.time + "s");
				run = false;
			}
			if (soufle < 12) {
				wait = 120;
				soufle += 40;
			}
			if (size > 0) {
				size -= 1;
				if (wait > 0)
					wait--;
				else
					soufle += 1;
				if (wait <= 0 && soufle >= 15 && Input.GetKeyDown (KeyCode.Space)) {
					size += 15;
					soufle -= 15;
				}
				//				Debug.Log ("wait:"+wait);
				//				Debug.Log ("soufle:"+soufle);
								Debug.Log ("size:"+size);
				transform.localScale = new Vector3 (size / 20, size / 20, size / 20);
			}
			if (this.size >= 110) {
				Destroy (gameObject);
				Debug.Log ("Balloon life time: " + (int)Time.time + "s");
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PongBall : MonoBehaviour {

	public float speed;
	private bool freeze = false;
	private int dirX;
	private int dirY;
	public Player p1, p2;

	void Start () {
		this.spawn ();
	}
	
	void Update () {
		if (!freeze) {
			if (this.transform.position.y >= 3.68)
				this.dirY = (this.dirY == -1 ? 1 : -1);
			else if (this.transform.position.y <= -3.67)
				this.dirY = (this.dirY == -1 ? 1 : -1);
			if (this.transform.position.x <= -3.46F && (this.transform.position.y <= p1.transform.position.y + 1.27F && this.transform.position.y >= p1.transform.position.y - 1.28F))
				this.dirX = (this.dirX == -1 ? 1 : -1);
			else if (this.transform.position.x >= 3.48F && (this.transform.position.y <= p2.transform.position.y + 1.27F && this.transform.position.y >= p2.transform.position.y - 1.28F))
				this.dirX = (this.dirX == -1 ? 1 : -1);
			if (this.transform.position.x <= -4.2F)
			{
				p2.addPoint();
				this.spawn();
			}
			else if (this.transform.position.x >= 4.2F)
			{
				p1.addPoint();
				this.spawn();
			}
			this.transform.Translate (new Vector2 (speed*dirX, speed*dirY));
		}
	}

	private void spawn()
	{
		this.transform.position = new Vector2 (0, 0);
		dirX = (Random.Range (0, 2) == 0 ? -1 : 1);
		dirY = (Random.Range (0, 2) == 0 ? -1 : 1);
		Debug.Log ("Player 1: "+ p1.getScore() +" | Player 2: " + p2.getScore());
	}
}

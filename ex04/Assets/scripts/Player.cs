﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public KeyCode up, down;
	private int score = 0;
	void Update () {

		if (Input.GetKey (this.up) && this.transform.position.y <= 3.0F)
			this.transform.Translate (new Vector2 (0, 0.06F));
		else if (Input.GetKey (this.down) && this.transform.position.y >= -3.0F)
			this.transform.Translate (new Vector2 (0, -0.06F));
	}

	public void addPoint()
	{
		this.score++;
	}

	public int getScore()
	{
		return this.score;
	}
}

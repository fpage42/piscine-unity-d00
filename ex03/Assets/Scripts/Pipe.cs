﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour {

	public float startSpeed;
	public int score = 0;
	private bool freeze = false;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!freeze) {
			this.transform.Translate (new Vector2 (-(startSpeed + (score * 0.01F)), 0));
			if (this.transform.position.x <= -6.0F) {
				this.transform.Translate (new Vector2 (12.0F, 0));
				this.score++;
			}
		}
		}

	public int getScore()
	{
		return this.score;
	}

	public void stop()
	{
		freeze = true;
	}
}

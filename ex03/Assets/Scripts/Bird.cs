﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {

	public float boostValue;
	public float fallValue;
	public float boost = 0.0F;
	public bool freeze = false;
	public Pipe p;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!freeze)
		{
			if (Input.GetKeyDown(KeyCode.Space))
				this.boost = this.boostValue;
			if (this.boost > 0.0F) {
				this.boost -= 0.01F;
			}
			this.transform.Translate (new Vector2 (0, -fallValue + boost));
			if ((p.transform.position.x <= -0.09F && p.transform.position.x >= -2.5) && (this.transform.position.y <= -1.1F || this.transform.position.y >= 0.85F))
				this.end ();
			if (this.transform.position.y <= -2.6F || this.transform.position.y >= 4.63F)
				this.end ();
		}			
	}

	private void end()
	{
		this.freeze = true;
		p.stop ();
		Debug.Log ("Score: " + p.getScore()*5);
		Debug.Log ("Time: " + (int)Time.time + "s");
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

	float vitesse = 0;
	public KeyCode key;

	void Update () {
		if (vitesse == 0) {
			vitesse = Random.Range (1.0F, 2.0F);
		}
		if (transform.position.y <= -6.0F) {
			Destroy(gameObject);
		}
		//Destroy(this);
		transform.Translate(new Vector2 (0.0F, -vitesse/10.0F));
		if (Input.GetKeyDown (key)) {
			Debug.Log ("Precision: " + (transform.position.y+4.0F));
			Destroy(gameObject);
		}
	}
}

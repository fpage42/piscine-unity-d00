﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour {

	public GameObject cube;

	private float spawnTime;
	private float timer;

	void Update () {
		if (timer >= spawnTime) {
			spawnTime = Random.Range (1.0F, 3.0F);
			timer = 0;
			Vector3 newPos = new Vector3 (transform.position.x, 3.0F, 0);
			GameObject.Instantiate (cube, newPos, Quaternion.identity);
		}
		timer += Time.deltaTime;
	}
}

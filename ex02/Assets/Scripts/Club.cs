﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Club : MonoBehaviour {

	public Ball b;
	private int lastLoad = 0;
	private int load = 0;
	void Start () {
	}
	
	void Update () {
		if (b.isStop ()) {
			this.lastLoad = this.load;
			if (Input.GetKey (KeyCode.Space) && b.isStop()) {
				this.load += 1;
			}
			if (this.load == this.lastLoad) {
				b.setSpeed (0.01F*this.load);
				this.lastLoad = 0;
				this.load = 0;
			}
				this.transform.position = new Vector2(-0.2F, b.transform.position.y - (0.1F + 0.04F * this.load));
		}		
	}
}

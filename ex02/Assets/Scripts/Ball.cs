﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	private float speed;
	private bool direction = false;
	private bool freeze = false;
	public float maxSpeed;

	void Start () {
		this.speed = 0.0F;
	}
	
	void Update () {
		if (!this.freeze && this.speed > 0.0F) {
			if (this.transform.position.y >= 4.69F) {
				this.direction = true;
			}
			if (this.transform.position.y <= -4.63F) {
				this.direction = false;
			}
			this.transform.Translate(new Vector2(0, speed * (direction ? -1:1)));
				this.speed -= 0.01F;
		}
		if (this.speed <= maxSpeed && this.transform.position.y >= 2.35F && this.transform.position.y <= 2.68F) {
			this.freeze = true;
			this.transform.position = new Vector2 (0, 7);
		}
	}

	public void setSpeed(float s)
	{
		this.speed = s;
	}

	public bool isStop()
	{
		return (speed <= 0.0F ? true : false);
	}
}
